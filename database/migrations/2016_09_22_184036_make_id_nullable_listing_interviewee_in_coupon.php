<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeIdNullableListingIntervieweeInCoupon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupons', function($table)
        {
            $table->integer('listing_id')->unsigned()->nullable()->change();
        });
        Schema::table('coupon_interviewee', function($table)
        {
            $table->integer('interviewee_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupons', function($table)
        {
            $table->integer('listing_id')->unsigned()->change();
        });
        Schema::table('coupon_interviewee', function($table)
        {
            $table->integer('interviewee_id')->unsigned()->change();
        });
    }
}
