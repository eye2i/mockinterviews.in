<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('user_subcategory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->integer('subcategory_id')->unsigned()->nullable();
            $table->foreign('subcategory_id')
                ->references('id')->on('subcategories')
                ->onUpdate('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade');

            $table->timestamps();
        });

        Schema::create('coupon_interviewee', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('used');
            $table->integer('coupon_id')->unsigned();
            $table->foreign('coupon_id')
                ->references('id')->on('coupons')
                ->onUpdate('cascade');
            $table->integer('interviewee_id')->unsigned();
            $table->foreign('interviewee_id')
                ->references('id')->on('interviewees')
                ->onUpdate('cascade');

            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('user_subcategory');
        Schema::drop('coupon_interviewee');
    }
}
