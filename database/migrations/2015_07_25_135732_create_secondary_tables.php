<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondaryTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade');
            $table->string('company_name');
            $table->string('title');
            $table->string('location');
            $table->text('description')->nullable();
            $table->date('start_date');
            $table->date('end_date');

            $table->timestamps();
        });

        Schema::create('educations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade');
            $table->string('institution');
            $table->string('degree');
            $table->date('start_date');
            $table->date('end_date');

            $table->timestamps();
        });


        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->timestamps();
        });


        Schema::create('subcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onUpdate('cascade');

            $table->timestamps();
        });


        Schema::create('listings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('price');
            $table->text('description');
            $table->boolean('disabled', 1)->default(false);
            $table->integer('subcategory_id')->unsigned();
            $table->foreign('subcategory_id')
                ->references('id')->on('subcategories')
                ->onUpdate('cascade');
            $table->integer('interviewer_id')->unsigned();
            $table->foreign('interviewer_id')
                ->references('id')->on('interviewers')
                ->onUpdate('cascade');

            $table->timestamps();
        });


        Schema::create('availabilities', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('start_timestamp');
            $table->timestamp('end_timestamp');
            $table->integer('interviewer_id')->unsigned();
            $table->foreign('interviewer_id')
                ->references('id')->on('interviewers')
                ->onUpdate('cascade');

            $table->timestamps();
        });


        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('interviewee_id')->unsigned();
            $table->foreign('interviewee_id')
                ->references('id')->on('interviewees')
                ->onUpdate('cascade');
            $table->integer('availability_id')->unsigned();
            $table->foreign('availability_id')
                ->references('id')->on('availabilities')
                ->onUpdate('cascade');
            $table->integer('listing_id')->unsigned();
            $table->foreign('listing_id')
                ->references('id')->on('listings')
                ->onUpdate('cascade');

            $table->timestamps();
        });


        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rating');
            $table->text('text');
            $table->integer('for_user_id')->unsigned();
            $table->foreign('for_user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade');
            $table->integer('by_user_id')->unsigned();
            $table->foreign('by_user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade');

            $table->integer('listing_id')->unsigned();
            $table->foreign('listing_id')
                ->references('id')->on('listings')
                ->onUpdate('cascade');

            $table->timestamps();
        });

        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('discount_percent');
            $table->string('code');
            $table->integer('count_left');
            $table->timestamp('start');
            $table->timestamp('end');
            $table->integer('listing_id')->unsigned();
            $table->foreign('listing_id')
                ->references('id')->on('listings')
                ->onUpdate('cascade');

            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('coupons');
        Schema::drop('reviews');
        Schema::drop('appointments');
        Schema::drop('availabilities');
        Schema::drop('listings');
        Schema::drop('subcategories');
        Schema::drop('experiences');
        Schema::drop('educations');
        Schema::drop('categories');
    }
}
