<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
//        Schema::table('users', function (Blueprint $table) {
//            $table->index('first_name');
//            $table->index('last_name');
//            $table->index('city');
//        });
//
//        Schema::table('experiences', function (Blueprint $table) {
//            $table->index('company_name');
//            $table->index('location');
//            $table->index('start_date');
//            $table->index('end_date');
//        });
//
//        Schema::table('educations', function (Blueprint $table) {
//            $table->index('institution');
//            $table->index('degree');
//            $table->index('start_date');
//            $table->index('end_date');
//        });
//
//        Schema::table('subcategories', function (Blueprint $table) {
//            $table->index('name');
//        });
//
//        Schema::table('reviews', function (Blueprint $table) {
//            $table->index('rating');
//        });
//
//        Schema::table('availabilities', function (Blueprint $table) {
//            $table->index('start_timestamp');
//            $table->index('end_timestamp');
//
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
//        Schema::table('users', function (Blueprint $table) {
//            $table->dropIndex('users_first_name_index');
//            $table->dropIndex('users_last_name_index');
//            $table->dropIndex('users_city_index');
//        });
//
//        Schema::table('experiences', function (Blueprint $table) {
//            $table->dropIndex('experiences_company_name_index');
//            $table->dropIndex('experiences_location_index');
//            $table->dropIndex('experiences_start_date_index');
//            $table->dropIndex('experiences_end_date_index');
//        });
//
//        Schema::table('educations', function (Blueprint $table) {
//            $table->dropIndex('educations_institution_index');
//            $table->dropIndex('educations_degree_index');
//            $table->dropIndex('educations_start_date_index');
//            $table->dropIndex('educations_end_date_index');
//        });
//
//        Schema::table('subcategories', function (Blueprint $table) {
//            $table->dropIndex('subcategories_name_index');
//        });
//
//        Schema::table('reviews', function (Blueprint $table) {
//            $table->dropIndex('reviews_rating_index');
//        });
//
//        Schema::table('availabilities', function (Blueprint $table) {
//            $table->dropIndex('availabilities_start_timestamp_index');
//            $table->dropIndex('availabilities_end_timestamp_index');
//
//        });

    }
}
