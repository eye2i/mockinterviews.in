/**
 * Created by chirag on 06/07/15.
 */
//Copied function
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

//Copied function
function getParameterByName(name, href) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(href);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function getBaseUrl() {
    var baseUrl = window.location.href;
    if (baseUrl.indexOf('#') != -1) {
        baseUrl = baseUrl.substring(0, baseUrl.indexOf('#'));
    }
    if (baseUrl.indexOf('?') != -1) {
        baseUrl = baseUrl.substring(0, baseUrl.indexOf('?'));
    }
    return baseUrl;
}


$(document).ready(function () {

    var query = $('#query');

    $('.btn.btn-default.right-menu-yellow-btn').on('click',function(){
       window.location = $(this).find("a").eq(0).attr('href');
    });

    $('#search-btn').click(function (e) {
        e.preventDefault();
        if (query.val() != null && query.val() != "") {
            var pathArray = location.href.split('/');
            var protocol = pathArray[0];
            var host = pathArray[2];
            var baseUrl = protocol + '//' + host;
            var finalUrl = baseUrl + '/listings?q=' + query.val();
            //window.location.hash = "q=" + query.val();
            window.location.replace(finalUrl);
        }
    });
    function slugify(text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');
    }

    $('.drop-menu.drop-menu-item').map(function () {
        var jthis = $(this);
        jthis.attr('data-cling', slugify(jthis.attr('data-cling')));
    });
    $('.dropdown-panel').map(function () {
        var jthis = $(this);
        jthis.attr('id', slugify(jthis.attr('id')));
    });

    var last_hovered;
    var header_second_bar_cached = $('.header-second-bar');
    var header_fixed_cached = $('.header-fixed');
    var header_topbar_cached = $('.header-topbar');
    var kek = $('.kek').prop('outerHTML');
    var header_menu_cached = $('.header-menu');


    $(window).on('scroll', function (e) {
        //var top = $(this).scrollTop();
        var left = $(this).scrollLeft();
        header_fixed_cached.css('left', -left);
//            if (top > 150) {
//                header_second_bar_cached.addClass('header-second-bar-hide');
//                header_topbar_cached.addClass('sticky');
//            }
//            else {
//                header_second_bar_cached.removeClass('header-second-bar-hide');
//                header_topbar_cached.removeClass('sticky');
//            }
    });

    $('.drop-menu-item').hover(function (e) {
        last_hovered = $(this);
        var cling_div = $('#' + $(this).data('cling'));
        cling_div.toggle();
    });

    $('.dropdown-panel').mouseover(function (e) {
        last_hovered.addClass('drop-menu-item-hover');
        $(this).css('display', 'block');
    });

    $('.dropdown-panel').mouseout(function (e) {
        last_hovered.removeClass('drop-menu-item-hover');
        $(this).css('display', 'none');
    })

});