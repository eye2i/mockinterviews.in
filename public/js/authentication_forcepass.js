/**
 * Created by paren on 9/16/2015.
 */
var change_check = function () {
    var old_correct = false;
    var new_correct = false;
    var cnew_correct = false;

    var $old = $('#old');
    var $new = $('#new');
    var $cnew = $('#cnew');
    var $submit = $('#submit');

    if ($old.val()) {
        var e1 = pass_check($old.val());
        if (e1 == 0) {
            old_correct = true;
        }
    }
    if ($new.val()) {
        var e2 = pass_check($new.val());
        if (e2 == 0) {
            new_correct = true;
        }
    }
    if ($cnew.val()) {
        var e3 = pass_check($cnew.val());
        if (e3 == 0) {
            cnew_correct = true;
        }
    }

    if (old_correct && new_correct && cnew_correct && $new.val() == $cnew.val() && $('#userid').val()) {
        $submit.removeClass('disabled');
        $submit.prop('disabled', false);
    }
    else {
        $submit.addClass('disabled');
        $submit.prop('disabled', true);
    }
};

$(document).ready(function () {
    $('#old').keyup(change_check);
    $('#new').keyup(change_check);
    $('#cnew').keyup(change_check);
    var url = window.location.href;
    var params = url.extract();
    if(params && params.id)
        $('#userid').val(params.id);
});
