/**
 * Created by paren on 9/16/2015.
 */
var contact_check = function () {
    var phone_correct = true;
    var name_correct = false;
    var email_correct = false;
    var messg_correct = false;

    var $contact = $('#contact');
    var $phone = $('#contact_phone');
    var $email = $('#contact_email');
    var $mssg = $('#contact_messg');
    var $name = $('#contact_name');

    if($name.val()) {
        name_correct = true;
    }
    if($email.val()) {
        email_correct = email_check($email.val());
    }
    if($phone.val()) {
        phone_correct = phone_check($phone.val());
    }
    if($mssg.val()) {
        messg_correct = true;
    }
    if(phone_correct && name_correct && email_correct && messg_correct) {
        $contact.prop('disabled', false);
        $contact.removeClass('disabled');
    }
    else {
        $contact.prop('disabled', true);
        $contact.addClass('disabled');
    }
};

$(document).ready(function () {
    var $contact = $('#contact');
    $contact.prop('disabled', true);
    $contact.addClass('disabled');
    $('#contact_name, #contact_email, #contact_phone, #contact_messg').keyup(contact_check);
    $(':input[type=number]').on('mousewheel', function(e){
        e.preventDefault();
    });
});