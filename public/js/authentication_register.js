/**
 * Created by paren on 9/16/2015.
 */
$(document).ready(function () {
    
    $('.btn-google,.btn-linkedin').click(function () {
        $("body").addClass('loading');
        $(".loading_modal").css('display', 'block');
    });

    var email_has_error = function () {

        if (!email_check($('#remail').val())) {
            $('#email_ig').removeClass('has-success').addClass('has-error');
            $('#remail').attr('title', 'Please enter a valid email').tooltip('fixTitle').tooltip('show');
        } else {
            $('#email_ig').removeClass('has-error').addClass('has-success');
            $('#remail').tooltip('destroy');
        }
    };

    var pass_hass_error = function () {
        var $pass = $('#rpassword');
        var error = pass_check($pass.val());
        //console.log(error);
        if (error != 0) {
            $('#pass_ig').removeClass('has-success').addClass('has-error');
            $pass.attr('title', error).tooltip('fixTitle').tooltip('show');
        }
        else{
            $('#pass_ig').removeClass('has-error').addClass('has-success');
            $('#rpassword').tooltip('destroy');
        }
    };

    var cpass_has_error = function () {
        if ($('#rpassword').val() != $('#crpassword').val()) {
            $('#cpass_ig').removeClass('has-success').addClass('has-error');
            $('#crpassword').attr('title', 'Passwords do not match').tooltip('fixTitle').tooltip('show');
        }
        else{
            $('#cpass_ig').removeClass('has-error').addClass('has-success');
            $('#crpassword').tooltip('destroy');
        }
    };

    var fname_has_error = function () {
        if (!name_check($('#fname').val())) {
            $('#fname_ig').removeClass('has-success').addClass('has-error');
            $('#fname').attr('title', 'Please enter a valid name').tooltip('fixTitle').tooltip('show');
        }
        else{
            $('#fname_ig').removeClass('has-error').addClass('has-success');
            $('#fname').tooltip('destroy');
        }
    };

    var lname_has_error = function () {
        if (!name_check($('#lname').val())) {
            $('#lname_ig').removeClass('has-success').addClass('has-error');
            $('#lname').attr('title', 'Please enter a valid name').tooltip('fixTitle').tooltip('show');
        }
        else{
            $('#lname_ig').removeClass('has-error').addClass('has-success');
            $('#lname').tooltip('destroy');
        }
    };

    var register_check = function () {
        var email_correct = false;
        var pass_correct = false;
        var cpass_correct = false;
        var fname_correct = false;
        var lname_correct = false;
        var $remail = $('#remail');
        var $rpassword = $('#rpassword');
        var $crpassword = $('#crpassword');
        var $fname = $('#fname');
        var $lname = $('#lname');
        if (email_check($remail.val())) {
            email_correct = true;
            $('#email_ig').removeClass('has-error').addClass('has-success');
            $remail.tooltip('destroy');
        } else {
            email_has_error();
        }

        var error = pass_check($rpassword.val());
        if (error == 0) {
            pass_correct = true;
            $('#pass_ig').removeClass('has-error').addClass('has-success');
            $rpassword.tooltip('destroy');
        } else {
            pass_hass_error();
        }

        if (pass_check($crpassword.val()) == 0) {
            cpass_correct = true;
            if ($crpassword.val() == $rpassword.val()) {
                $('#cpass_ig').removeClass('has-error').addClass('has-success');
                $crpassword.tooltip('destroy');
            } else {
                cpass_has_error();
            }
        }

        if (name_check($fname.val())) {
            fname_correct = true;
            $('#fname_ig').removeClass('has-error').addClass('has-success');
            $fname.tooltip('destroy');
        } else {
            fname_has_error();
        }


        if (name_check($lname.val())) {
            lname_correct = true;
            $('#lname_ig').removeClass('has-error').addClass('has-success');
            $lname.tooltip('destroy');
        } else {
            lname_has_error();
        }
        return (email_correct && pass_correct && cpass_correct && ($rpassword.val() == $crpassword.val()) && fname_correct && lname_correct);
    };

    var $remail = $('#remail');
    var $fname = $('#fname');
    var $lname = $('#lname');
    $remail.keyup(email_has_error);
    $('#rpassword').keyup(pass_hass_error);
    $('#crpassword').keyup(cpass_has_error);
    $fname.change(fname_has_error);
    $lname.change(lname_has_error);
    $(':input[type=number]').on('mousewheel', function (e) {
        e.preventDefault();
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('#register').click(function(){

        if(register_check()){
            $("body").addClass('loading');
            $(".loading_modal").css('display','block');
            $('#register_form').submit();
        }
        else{
            $('html, body').animate({
                scrollTop: $("#register_form").offset().top - 200
            }, 1000);
        }
    });
});