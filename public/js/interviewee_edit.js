/**
 * Created by chirag on 26/07/15.
 */
$(window).scroll(function() {
    if ($(this).scrollTop() > 100){
        $('.nav-row-brand').addClass("sticky");
    }
    else{
        $('.nav-row-brand').removeClass("sticky");
    }
});

jQuery.easing.easeOutQuart = function (x, t, b, c, d) {
    return -c * ((t=t/d-1)*t*t*t - 1) + b;
};



$('.sandbox-container').datepicker({

    format: "mm-yyyy",
    startView: "months",
    minViewMode: "months"

});


$(document).ready(function(){







    $('[data-toggle="tooltip"]').tooltip();

    $('#slideshow').serialScroll({
        items:'.cal-item',
        prev: '.als-prev',
        next: '.als-next',
        start:0, //as we are centering it, start at the 2nd
        duration:60,
        force:true,
        stop:true,
        lock:false,
        easing: 'easeOutQuart',
        cycle:false, //don't pull back once you reach the end
        jump: true,
        step: 6
    });

    $(".js-example-tokenizer").select2({
        tags: true
    });

    $(".js-example-responsive").select2({
        placeholder: "Subcategory"
    });



    $(".js-example-tokenizer").on("select2:select", function (e) {
        console.log("select2:select", $(this).val()[0]);
        var skill = $(this).val()[0];
        $(this).val(null).trigger("change");
        $(this).select2('open');
        //$(this).trigger("open");
        if(skill!='') {
            $("<div class = 'skill-tag' id = 'clonetag' style = 'display: inline-block; margin: 5px;'> <a href='/listings?q=" + skill + "#q=" + skill + "'>" + skill + "</a> <a style='padding-left: 5px;cursor: pointer;' class = 'tagRemove'  tag='" + skill + "' other = '1'>  x</a>  </div>").appendTo($('#skillContainer'));
        }
    });

    $(".js-example-tokenizer").on("select2-blur", function (e) {
        console.log("gay");
    });

    $(".addSkillBtn2").click(function(e){
        $(".js-example-tokenizer").select2('open');
    });

    //$(".select2-results").on('close', function () {
    //    console.log("sheit");
    //    console.log($('.select2-search__field').val());
    //    $('.select2-search__field').attr('tabindex', -1);
    //    return false;
    //   // self.$search.val('');
    //});
    //
    //$('.select2-search__field').on('blur',function(e)
    //{
    //    return false;
    //});


    $('#rating-input').rating({
        showClear: false,
        showCaption: false,
        step :1
    });

    $('.add').click(function(){
        console.log($(this).attr('rating'));
        var cpy = $('#myModal');
        cpy.removeClass('hide');
        $(this).parent().parent().parent().append(cpy);
        $('#rating-input').rating('update', $(this).attr('rating'));
        if(!$(this).attr('rating'))
        {
            $('#rating-input').rating('update', 1);
        }
        $('#review').val($(this).attr('text'));
        $('#submitbtn').attr('for',$(this).attr('for'));
        $('#submitbtn').attr('by',$(this).attr('by'));
        $('#submitbtn').attr('listing',$(this).attr('listing'));
    });

    $('.upd').click(function(){
        console.log($(this).attr('rating'));
        var cpy = $('#myModal');
        cpy.removeClass('hide');
        $(this).parent().parent().parent().append(cpy);
        $('#rating-input').rating('update', $(this).attr('rating'));
        $('#review').val($(this).attr('text'));
        $('#submitbtn').attr('for',$(this).attr('for'));
        $('#submitbtn').attr('by',$(this).attr('by'));
        $('#submitbtn').attr('listing',$(this).attr('listing'));
    });



    $('#submitbtn').click(function(e){

        e.preventDefault();
        var by = $(this).attr('by');
        var fr = $(this).attr('for');
        var listing = $(this).attr('listing');
        console.log(by+""+fr+""+listing);
        $.ajax({
            url: 'http://'+window.location.href.split('/')[2]+'/addReview',
            type: 'GET',
            data: {
                rating : $('#rating-input').rating()['0']['value'],
                text : $('#review').val(),
                by : by,
                for : fr,
                listing : listing
            },
            success: function(response){
                if(response){
                    if(response==="true"){
                        console.log($('#listing'+listing));
                        $('#listing'+listing).text("Update Review");
                        $('#listing'+listing).addClass('upd');
                        $('#listing'+listing).removeClass('add');
                        $('#listing'+listing).attr('rating',$('#rating-input').rating()['0']['value']);
                        $('#listing'+listing).attr('text',$('#review').val());
                        $('#myModal').addClass('hide');
                    }
                }
            }});


    });

    $('#cancelbtn').click(function(e){
        $('#myModal').addClass('hide');
    });

    $('.rating-container').attr('data-content','★ ★ ★ ★ ★');
    $('.rating-stars').attr('data-content','★ ★ ★ ★ ★');


    //$('#addSkillBtn').click(function(e){
    //
    //    console.log($('.select2-results__option .select2-results__option--highlighted').text());
    //    //console.log($(".select2-search__field"));
    //    //var skill = $(".select2-search__field").val();
    //    //console.log(skill);
    //    //if(skill!='') {
    //    //    $("<div class = 'skill-tag' id = 'clonetag' style = 'display: inline-block; margin: 5px;'> <a href='/listings?q=" + skill + "#q=" + skill + "'>" + skill + "</a> <a style='padding-left: 5px;cursor: pointer;' class = 'tagRemove'  tag='" + skill + "' other = '1'>  x</a>  </div>").appendTo($('#skillContainer'));
    //    //}
    //    //$(".js-example-tokenizer").val(null).trigger("change");
    //
    //});

    $('div').on('click', 'a.tagRemove',function(e){
        $(this).parent().remove();
    });

    //$('#sandbox-container').datepicker();

    $('#addExp').click(function(e){
        console.log('clicked');
        $("<div class='row timeline-details bottomSep'> <div class = 'row'> <div class = 'col-xs-11'> <input style='width: 250px;' class='form-control' type = 'text' id='companyName' placeholder='Company Name' value=''> </div> <div class = 'col-xs-1'> <a class='pull-right btn shortlist-btn delExp'><i class='fa fa-times'></i></a> </div> </div> <div class='row interviewer-current-work' style='padding-top: 5px!important;'> <div><input style='width: 200px;' class='form-control input-sm' type = 'text' id='workTitle' placeholder='Work Title' value=''></div> </div> <div class='row interviewer-current-work' style='padding-top: 5px!important;'> <div><input style='width: 200px;' class='form-control input-sm' type = 'text' id='location' placeholder='Location' value=''></div> </div> <div class='row interviewer-current-work col-xs-10 pull-left' style='padding-left: 0px!important;padding-top: 5px;'> <div class='input-daterange input-group sandbox-container'> <input type='text' class='input-sm form-control' name='start' value='' placeholder='start date'/> <span class='input-group-addon'>to</span> <input type='text' class='input-sm form-control' name='end' value='' placeholder='end date'/> </div> </div> <div class='row interviewer-current-work col-xs-10 pull-left' style='padding-left: 0px!important;padding-top: 5px;'> <textarea class='form-control' rows='3' id='bio' placeholder='Bio'></textarea> </div> </div> ").appendTo($('#workExContainer'));
        $('.sandbox-container').datepicker({

            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months"

        });
    });

    $('#addEdu').click(function(e){
        console.log('clicked');
        $("<div class='row timeline-details bottomSep'> <div class = 'row'> <div class = 'col-xs-11'> <input style='width: 250px;' class='form-control' type = 'text' id='institution' placeholder='Institution' value=''> </div> <div class = 'col-xs-1'> <a class='pull-right btn shortlist-btn delEdu'><i class='fa fa-times'></i></a> </div> </div> <div class='row interviewer-current-work' style='padding-top: 5px!important;'> <div><input style='width: 200px;' class='form-control input-sm' type = 'text' id='degree' placeholder='Degree' value=''></div> </div> <div class='row interviewer-current-work col-xs-10 pull-left' style='padding-left: 0px!important;padding-top: 5px;'> <div class='input-daterange input-group sandbox-container'> <input type='text' class='input-sm form-control' name='start' value='' placeholder='start date'/> <span class='input-group-addon'>to</span> <input type='text' class='input-sm form-control' name='end' value='' placeholder='end date'/> </div> </div> </div> ").appendTo($('#eduContainer'));
        $('.sandbox-container').datepicker({

            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months"

        });
    });

    $('#addLis').click(function(e){
        console.log('clicked');
        $("<div class='row timeline-details newLis bottomSep'><div class = 'row'><div class = 'col-xs-11'><input style='width: 250px;' class='form-control' type = 'text' id='listingCat' placeholder='Subcategory Name' value=''></div><div class = 'col-xs-1'><a class='pull-right btn shortlist-btn newdelLis'><i class='fa fa-times'></i></a></div></div><div class='row interviewer-current-work' style='padding-top: 5px!important;'><div><input style='width: 200px;' class='form-control input-sm' type = 'text' id='listingPrice' placeholder='Price' value=''></div></div><div class='row interviewer-current-work col-xs-10 pull-left' style='padding-left: 0px!important;padding-top: 5px;'><textarea class='form-control' rows='3' id='bio' placeholder='Description'></textarea> </div> </div> ").appendTo($('#listingContainer'));
    });

    $('div').on('click','a.delExp',function(){
       $(this).parent().parent().parent().remove();
    });

    $('div').on('click','a.delEdu',function(){
        $(this).parent().parent().parent().remove();
        //console.log('gay');
    });

    $('div').on('click','a.newdelLis',function(){

        $(this).parent().parent().parent().remove();
    });

    $('div').on('click','a.delLis',function(){
        console.log($(this).parent().parent().parent().find(':input'));
        if($(this).text() == "Enable"){
            $(this).parent().parent().parent().removeClass("disabled");
            $(this).parent().parent().parent().find(':input').each(function() {
               $(this).attr('disabled',false);
            });
            $(this).text('Disable');
            return false;
        }
        else {
            $(this).parent().parent().parent().addClass("disabled");
            $(this).parent().parent().parent().find(':input').removeClass("disabled");
            $(this).text('Enable');
            $(this).parent().parent().parent().find(':input').each(function() {
                $(this).attr('disabled',true);
            });
            return false;
        }

    });







    //parens js code for crop
    $("#inputImage").on('change',function () {
        var files = !!this.files ? this.files : [];
        if(!files.length || !window.FileReader) return;
        if(/^image/.test(files[0].type)){
            var reader = new FileReader;
            reader.readAsDataURL(files[0]);
            reader.onloadend = function () {
                $('#imageToCrop').prop('src', this.result);
                $('#preview').empty();
                $('#modal_crop').modal('show');
            }
        }
    });
    var $image = $('#cropper_image > img'),
        cropBoxData,
        canvasData;

    var cropdata= 'false';

    $('#modal_crop').on('shown.bs.modal', function () {
        $image.cropper({
            aspectRatio: 1 / 1,
            autoCropArea: 0.5,
            preview: ".img-preview",
            movable: false,
            zoomable: false,
            rotatable: false,
            built: function () {
                // Strict mode: set crop box data first
                $image.cropper('setCropBoxData', cropBoxData);
                $image.cropper('setCanvasData', canvasData);
            }
        });
    }).on('hidden.bs.modal', function () {
        cropBoxData = $image.cropper('getCropBoxData');
        canvasData = $image.cropper('getCanvasData');
        var $data = $image.cropper('getData');
        cropdata =  JSON.stringify($data);
        var $img = $('.img-preview').children();
        $image.cropper('destroy');
        $('.img-preview').append($img);
    });




    //Remove duplicates, this is a really bad hack

    var usedNames = {};
    $("#skillselect> option").each(function () {
        if(usedNames[this.text]) {
            $(this).remove();
        } else {
            usedNames[this.text] = this.value;
        }
    });




    $('#pd_newpassword').on('change',function(){

        if(pass_check($('#pd_newpassword').val())!='0'){
            //Show tooltip
        }

    });


    $('#pd_confirmnewpassword').on('change',function(){

        if(pass_check($('#pd_newpassword').val())!='0' || ($('#pd_newpassword').val()) != ($('#pd_confirmnewpassword').val())){
            //Show tooltip
        }

    });





    $('#saveedit').on('click',function(){
        //Show Progress indicator -- ask chirag


        var fn = $('#pd_firstname').val();
        var mn = $('#pd_middlename').val();
        var ln = $('#pd_lastname').val();
        var ph = $('#pd_phoneno').val();
        var ct = $('#pd_city').val();
        var bio = $('#pd_bio').val();
        var op = $('#pd_oldpassword').val();
        var np = $('#pd_newpassword').val();
        var ncp = $('#pd_confirmnewpassword').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        if(np!='' && ncp==np){
            pass_c = pass_check(np);
            if(pass_c===0){
                //Call Ajax for change password, send user_id, pass, newpass, oldpass
                $.ajax({
                    url: 'http://'+window.location.href.split('/')[2]+'/changePwd',
                    type: 'POST',
                    data: {
                        op : op,
                        np : np
                    },
                    success: function(response){
                        console.log(response);
                        if(response){
                            if(response==="true"){
                                console.log("changed");
                            }
                            else{
                                console.log(" not changed " + response );
                            }
                        }
                    },
                    error: function (response) {
                        console.log('Error:',response);
                    }
                });
            }
            else{
                //Show error
                console.log("pass check failed" + pass_c)
            }
        }
        else if (np!=''){
            console.log("password didn't match")
        }

        //Resave personal Info
        $.ajax({
            url: '/resavePersonal',
            type: 'POST',
            data: {
                fn:fn,
                mn:mn,
                ln:ln,
                ph:ph,
                ct:ct,
                bio:bio
            },
            success: function(response){
                if(response){
                    if(response==="true"){
                        console.log("resaved");
                    }
                    else{
                        console.log(" not resaved");
                    }
                }
            },
            error: function (response) {
                console.log('Error:',response);
            }
        });
        //call Ajax to update fn,mn,ln, phone_no, city and bio

        var imageFile = !!$('#inputImage').files ? $('#inputImage').files : [];
        var resumeFile = !!$('#inputFile').files ? $('#inputFile').files : [];

        if(!imageFile.length){
            sendImage(imageFile[0]);
        }

        if(!resumeFile.length){
            sendResume(resumeFile[0]);
        }




        //Check Resume and Picture for change

        $('.skill-tag').each(function(){
            $this.find("a").first().val();


        });

        //Iterate New added skills
        //ajaxCall to send All Skills, (this deletes old skills, and then inserts new ones)


        //New WorkEx, Edu(class = ), Listing(class = newLis)

        //Check all listing for Enable/Disable
    });



});


$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

    if($(e.target).attr("href").indexOf('availability')!=-1)
    {
        console.log("Als Event");

    }








});







//DO NOT SEND TO CHIRAG, ALREADY PRESENT IN HIS AUTHENTICATION.JS

function email_check (email) {
    var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if(!re.test(email)){
        return false;
    }
    return true;
}

function pass_check (password) {
    var error = "Your password must ";
    var e = 0
    if(password.length < 8){
        if(e==0 || e==1) {
            error += "be at least 8 characters long"
            e = 1;
        }
    }
    if(password.search(/[a-z]/i)<0){
        if(e==0){
            error += "contain at least one alphabet";
            e = 2;
        }
        else{
            error += ", contain at least one alphabet";
            e = 3;
        }
    }
    if(password.search(/[0-9]/)<0){
        if(e==0){
            error += "contain at least one digit";
        }
        else if(e==1){
            error += ", contain at least one digit";
        }
        else if(e==2||e==3){
            error += "and one digit";
        }
    }
    if(e==0){
        return 0;
    }
    else{
        return error;
    }
}

function dob_check (dob) {
    return /^\d\d\/\d\d\/\d\d\d\d$/.test(dob);
}

function phone_check (phone) {
    return /^[0-9]{10}$/.test(phone);
}

function name_check (name) {
    var re = /^[a-zA-Z]+$/;
    return (re.test(name));
}

function bio_check (bio) {
    return /^.{140,500}$/.test(bio);
}


function sendFile(file) {
    data = new FormData();
    data.append("upload", file);
    $.ajax({
        data: data,
        type: "POST",
        url: 'http://uploads.im/api', //Change
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            image_url = "-1";
            if(response.status_code == 200)
            {
                image_url=response.data.img_url;
            }
            else
            {
                //error
            }
            console.log(image_url);
            if(image_url=="-1")
            {
                //eroor
            }
            else
            {
                $('#summernote').summernote('editor.insertImage', image_url);
            }

        }
    });
}

function sendImage(file) {
    data = new FormData();
    data.append("upload", file);
    $.ajax({
        data: data,
        type: "POST",
        url: 'http://'+window.location.href.split('/')[2]+'/sendImage',
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {

            if(response){
                if(response==="true"){
                    console.log('Image Sent');
                }
            }
        }
    });
}

function sendResume(file) {
    data = new FormData();
    data.append("upload", file);
    $.ajax({
        data: data,
        type: "POST",
        url: 'http://'+window.location.href.split('/')[2]+'/sendResume',
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            if(response){
                if(response==="true"){
                    console.log('Resume Sent');
                }
            }

        }
    });
}