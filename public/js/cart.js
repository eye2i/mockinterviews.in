/**
 * Created by chirag on 13/06/16.
 */
$(document).ready(function(){
    setTimeout(function () {
        $("body").addClass("loading");
        var el = document.getElementById('alwaysFetch');
        el.value = el.value ? location.reload() : true;
    }, 0);
    $('[data-toggle="tooltip"]').tooltip();
    $(document).on({
        ajaxStart: function () {
            $("body").addClass("loading");
        },
        ajaxStop: function () {
            $("body").removeClass("loading");
        }
    });

    $('#cart').prop('href', 'checkout');
    $('#cart').text('Checkout');

    $('.alert').on('close.bs.alert', function (e){
        $.ajax({
            url: 'removefromcart',
            type: 'POST',
            data: {
                value:$(this).attr('id').split('-')[1]
            },
            success: function(response){
                if(response == 0){
                    $('.container').append($('<center><i><h4>Your Shortlist is empty!</h4></i></center><div class="row" id=""><a class="col-xs-4 col-xs-offset-4 btn btn-default yellow-btn" id="" href="/listings">Back to Listings</a></div>'));
                }
            }
        });
    });
});