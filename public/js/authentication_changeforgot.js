/**
 * Created by paren on 9/16/2015.
 */
var forgot_link = function () {
    var $cpnew = $('#cpnew');
    var $ccpnew = $('#ccpnew');
    if ($('#hash').val() && $('#userid').val() && pass_check($cpnew.val()) == 0 && pass_check($ccpnew.val()) == 0
        && $cpnew.val() == $ccpnew.val()) {
        $('#cpsubmit').removeClass('disabled');
    } else {
        $('#cpsubmit').addClass('disabled');
    }
};

$(document).ready(function () {
    $('#cpnew').keyup(forgot_link);
    $('#ccpnew').keyup(forgot_link);
    var url = window.location.href;
    if(params && params.email){
        var params = url.extract();
        var s = params.email.split('%40');
        $('#userid').val(s[0].concat('@', s[1]));
    }
    $('#hash').val(params.hash);
});
