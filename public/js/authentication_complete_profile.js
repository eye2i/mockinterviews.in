/**
 * Created by chirag on 03/07/16.
 */
/**
 * Created by paren on 9/16/2015.
 */
$(document).ready(function () {

    var phone_has_error = function () {
        if (!phone_check($('#phone').val())) {
            $('#phone_ig').removeClass('has-success').addClass('has-error');
            $('#phone').attr('title', 'Please enter a valid phone number').tooltip('fixTitle').tooltip('show');
        }
        else{
            $('#phone_ig').removeClass('has-error').addClass('has-success');
            $('#phone').tooltip('destroy');
        }
    };

    var city_has_error = function () {
        if (!city_check($('#city').val())) {
            $('#city_ig').removeClass('has-success').addClass('has-error');
            $('#city').attr('title', 'Please enter a valid city').tooltip('fixTitle').tooltip('show');
        }
        else{
            $('#city_ig').removeClass('has-error').addClass('has-success');
            $('#city').tooltip('destroy');
        }
    };

    var date_has_error = function () {
        if (!dob_check($('#date').val())) {
            $('#date_ig').removeClass('has-success').addClass('has-error');
            $('#date').attr('title', 'Date must be of format DD/MM/YYYY').tooltip('fixTitle').tooltip('show');
        }
        else{
            $('#date_ig').removeClass('has-error').addClass('has-success');
            $('#date').tooltip('destroy');
        }
    };

    var bio_has_error = function () {
        if (!bio_check($('#bio').val())) {
            $('#bio_ig').removeClass('has-success').addClass('has-error');
            $('#bio').attr('title', 'Summary should be atleast 140 characters long and 5000 characters at maximum').tooltip('fixTitle').tooltip('show');
        }
        else{
            $('#bio_ig').removeClass('has-error').addClass('has-success');
            $('#bio').tooltip('destroy');
        }
    };

    var register_check = function () {
        var phone_correct = false;
        var city_correct = false;
        var dob_correct = false;
        var bio_correct = false;
        var $phone = $('#phone');
        var $city = $('#city');
        var $date = $('#date');
        var $bio = $('#bio');
        if(phone_check($phone.val())) {
            phone_correct = true;
        }
        else{
            phone_has_error();
        }
        if (city_check($city.val())) {
            city_correct = true;
            $('#city_ig').removeClass('has-error').addClass('has-success');
            $city.tooltip('destroy');
        }
        else{
            city_has_error();
        }
        if (dob_check($date.val())) {
            dob_correct = true;
            $('#date_ig').removeClass('has-error').addClass('has-success');
            $date.tooltip('destroy');
        }
        else{
            date_has_error();
        }
        if(bio_check($bio.val())) {
            bio_correct = true;
            $('#bio_ig').removeClass('has-error').addClass('has-success');
            $bio.tooltip('destroy');
        }
        else{
            bio_has_error();
        }

        return (phone_correct && city_correct && dob_correct && bio_correct);
    };

    var $date = $('#date');
    $date.datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#phone').change(phone_has_error);
    $('#city').change(city_has_error);
    $date.change(date_has_error);
    $('#bio').change(bio_has_error);
    $(':input[type=number]').on('mousewheel', function (e) {
        e.preventDefault();
    });

    $('#save').click(function(){
        if(register_check()){
            $("body").addClass('loading');
            $(".loading_modal").css('display','block');
            $('#save_profile_form').submit();
        }
        else{
            $('html, body').animate({
                scrollTop: $("#save_profile_form").offset().top - 200
            }, 1000);
        }
    });
});


