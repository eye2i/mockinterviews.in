<?php
namespace App;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;
use Log;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
class User extends SleepingOwlModel implements AuthenticatableContract, \SleepingOwl\Models\Interfaces\ModelWithFileFieldsInterface
{
    //
    use Authenticatable;
    protected $table = 'users';
    protected $fillable = ['first_name','middle_name','last_name','email','phone_no','password','city','bio','dob','gender','resume_url','profile_image_url','skills','activated','google_access_token','linkedin_access_token'];
    public $timestamps = true;
    public function toArray(){
        $array = parent::toArray();
        $array['photo']=$this->profile_image_url->link();
        return $array;
    }

    public function OAuth(){
        return $this->belongsTo('App\Social_Auth','social_auth_id');
    }

    public function experience(){
        return $this->hasMany('App\Experience','user_id');
    }

    public function education(){
        return $this->hasMany('App\Education','user_id');
    }

    public function skills(){
        return $this->belongsToMany('App\SubCategory','user_subcategory', 'user_id', 'subcategory_id');
    }

    public function reviewsFor(){
        return $this->hasMany('App\Review','for_user_id');
    }

    public function reviewsBy(){
        return $this->hasMany('App\Review','by_user_id');
    }

    public function skillPairs(){
        return $this->hasMany('App\User_Subcategory','user_id');
    }

    public function extraSkills(){
        return $this->skillPairs()->whereNull('subcategory_id');
    }

    public function getFileFields(){

        return [
            'resume_url' => ['resume/', function($directory, $originalName, $extension)
            {
                if($extension=='pdf'||$extension=='doc'||$extension=='docx')
                    return $this->id.'_resume.'.$extension;
                else
                    return 'Error.ReUploadNotCorrectFormat';
            }
            ],
            'profile_image_url' => ['profile_images/', function($directory, $originalName, $extension)
            {
                if($extension=='jpg'||$extension=='jpeg'||$extension=='png')
                    return $this->id.'_profile_image.'.$extension;
                else
                    //return '0_profile_image.jpg';
                    return 'Error.ReUploadNotCorrectFormat';
            },
            ]

        ];
    }

    public static function getList(){
        $data = array();
        foreach(User::all() as $i) {
            $data += array($i->id => $i->first_name.' '.$i->last_name);
        }
        return $data;

    }

    public function setSkillsAttribute($skills){
        $this->skills()->detach();
        if ( ! $skills) return;
        if ( ! $this->exists) $this->save();
        $this->skills()->attach($skills);
    }

    public function getDates(){
        return array_merge(parent::getDates(), ['dob']);
    }

    public function hasReviewedListing($listing){
        $allrevs = $this->reviewsBy;
        foreach($allrevs as $rev){

            if($rev->listing_id == $listing->id)
            {
                Log::error("true");
                return true;
            }
        }
        Log::error("false");
        return false;
    }

    public function getReviewForListing($listing){
        $allrevs = $this->reviewsBy;
        foreach($allrevs as $rev){

            if($rev->listing_id == $listing->id)
            {
                return $rev;
            }
        }

        return false;
    }

}
