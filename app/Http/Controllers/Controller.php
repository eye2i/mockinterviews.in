<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController{
    use DispatchesJobs, ValidatesRequests;

    public static $minimumProfileCompletionRedirectPath='/completeprofile';
    public static function isMinimumProfileComplete(){
        if(Auth::check()){
            $user = Auth::user();
            if ($user->first_name === null || $user->last_name === null || $user->dob === null || $user->phone_no === null || $user->bio === null || $user->city === null){
                return false;
            }
            else{
                return true;
            }
        }
        return true;
    }
}
