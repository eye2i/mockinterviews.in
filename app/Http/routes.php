<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('listings');
});

//auth
Route::get('login', 'AuthenticationController@getLogin');
Route::post('login', 'AuthenticationController@postLogin');
Route::get('logout', 'AuthenticationController@getLogout');
Route::get('register/interviewee', 'AuthenticationController@getRegisterInterviewee')->name('register_interviewee');
Route::get('register/interviewer', 'AuthenticationController@getRegisterInterviewer');
Route::get('register', function () { return redirect()->route('register_interviewee'); });
Route::post('register', 'AuthenticationController@postRegister');
Route::get('forgot_password', 'AuthenticationController@getForgot');
Route::post('forgot_password', 'AuthenticationController@postForgot');
Route::get('forgot_password/link', 'AuthenticationController@getForgotLink');
Route::post('forgot_password/link', 'AuthenticationController@postForgotLink');
Route::get('password_change', 'AuthenticationController@getForcePasswordChange');
Route::post('password_change', 'AuthenticationController@postForcePassword');
Route::get('register/confirm', 'AuthenticationController@getActivation');

//social_auth
Route::get('auth/redirect/{register_type}/{provider}', ['uses' => 'AuthenticationController@redirectToProvider', 'as' => 'auth']);
Route::get('auth/{provider}', 'AuthenticationController@handleProviderCallback');

//complete_profile
Route::get('completeprofile','AuthenticationController@getMinimumCompleteProfile');
Route::post('completeprofile','AuthenticationController@postMinimumCompleteProfile');
//extras
Route::any('contact', 'AuthenticationController@sendContactMail');
Route::any('about', 'AuthenticationController@getAbout');

//cart
Route::get('shortlist', 'ListingController@getCart');
Route::post('removefromcart', 'ListingController@unsetCart');
Route::post('addtocart', 'AuthenticationController@setSession');
Route::get('compare', 'CompareController@compare');

//home
Route::get('listings', 'ListingController@getListingPage');
Route::get('morelistings', 'ListingController@getMoreListings');

//profiles
Route::get('interviewer/profile/{id}', ['uses' => 'AuthenticationController@getInterviewerProfile']);
Route::get('interviewee/profile/{id}', ['uses' => 'AuthenticationController@getIntervieweeProfile']);
Route::get('interviewer/edit/{id}', ['uses' => 'ListingController@editInterviewerProfile']);
Route::get('interviewee/edit/{id}', ['uses' => 'ListingController@editIntervieweeProfile']);
Route::get('addreview', 'ListingController@addReview');
Route::post('/resavePersonal', 'ListingController@resaveInterviewee');
Route::post('/resaveWorkEx', 'ListingController@resaveWorkEx');
Route::post('/resaveEdu', 'ListingController@resaveEdu');
Route::post('/saveListings', 'ListingController@saveListings');
Route::post('/changePwd', 'ListingController@changePwd');
Route::get('compare', 'CompareController@compare');
Route::get('dashboard', 'AuthenticationController@getDashboard');
Route::post('saveavailabilities', 'AuthenticationController@saveAvailabilities');

//payment
Route::post('checkout', 'CheckoutController@startCheckoutFlow');
Route::post('startcheckout', 'CheckoutController@ccavenueRequestHandler');
Route::any('checkouthandler', 'CheckoutController@ccavenueResponseHandler');
Route::get('/getcoupon/{code}', 'CheckoutController@CouponHandler');