<?php

namespace App\Console\Commands;

use App\Appointment;
use App\Availability;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteStaleAvailabilities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'availabilities:staledelete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes all stale availabilities from database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Availability::where('end_timestamp', '<', Carbon::now())->whereNotIn('id', Appointment::all()->lists('availability_id')->toArray())->delete();
        $this->info('Stale Availabilities were deleted successfully!');
    }
}
