<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;

class Coupon extends SleepingOwlModel
{
    //
    protected $table = 'coupons';
    protected $fillable = ['discount_percent','code','count_left','start','end','listing_id','interviewees','intervieweePairs','orders'];
    public $timestamps = true;

    public function listing(){
        return $this->belongsTo('App\Listing','listing_id');
    }

    public function interviewees(){
        return $this->belongsToMany('App\Interviewee','coupon_interviewee', 'coupon_id', 'interviewee_id');
    }

    public function intervieweePairs(){
        return $this->hasMany('App\Coupon_Interviewee','coupon_id');
    }

    public function orders(){
        return $this->hasMany('App\Order','coupon_id');
    }

    public function setIntervieweesAttribute($interviewees)
    {
        $this->interviewees()->detach();
        if ( ! $interviewees) return;
        if ( ! $this->exists) $this->save();
        $this->interviewees()->attach($interviewees);
    }

    public static function getList(){
        $data = array();
        foreach (Coupon::all() as $i)
        {
            if($i->listing)
                $data += array($i->id => $i->code.' for '.$i->listing->subcategory->name);
            else
                 $data += array($i->id => $i->code.' for all Listings');
        }
        return $data;

    }

}
