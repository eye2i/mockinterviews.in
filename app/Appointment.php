<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\SleepingOwlModel;

class Appointment extends SleepingOwlModel
{
    //
    protected $table = 'appointments';
    protected $fillable = ['availability_id','interviewer_id','interviewee_id','listing_id','order_id'];
    public $timestamps = true;
//    public function interviewer(){
//        return $this->belongsTo('App\Interviewer','interviewer_id');
//    }
    public function interviewee(){
        return $this->belongsTo('App\Interviewee','interviewee_id');
    }
    public function availability(){
        return $this->belongsTo('App\Availability','availability_id');
    }
    public function listing(){
        return $this->belongsTo('App\Listing','listing_id');
    }
    public function order(){
        return $this->hasOne('App\Order','appointment_id');
    }

    public static function getList(){
        $data = array();
        foreach (Appointment::all() as $i)
        {
            $data += array($i->id => $i->interviewee->user->first_name.' for '.$i->listing->subcategory->name.' by '.$i->listing->interviewer->user->first_name);
        }
        return $data;

    }
}
