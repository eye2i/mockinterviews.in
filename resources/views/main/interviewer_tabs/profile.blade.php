<div class="tab-pane active" id="tab_profile">
    <div class="col-xs-10 col-xs-offset-1">
        <div class="row section-panel">
            <div class="section-title">
                Summary
            </div>
            <div class="section-body">
                {{$interviewer->user->bio}}
            </div>
        </div>

        <div class="row section-panel">
            <div class="section-title">
                Skills
            </div>
            <div class="section-body">
                @foreach($interviewer->user->skills as $skill)
                    <a class="skill-tag" href="{{ action('ListingController@getListingPage') }}?subcategories={{$skill->name}}">{{$skill->name}}</a>
                @endforeach
                @foreach($interviewer->user->extraSkills as $skill)
                    <a class="skill-tag" href="{{ action('ListingController@getListingPage') }}?q={{$skill->name}}">{{$skill->name}}</a>
                @endforeach
            </div>
        </div>


        <div class="row">
            <div class="col-xs-5">
                <div class="row section-panel">
                    <div class="section-title">
                        Work Experience
                    </div>
                    <div class="section-body">
                        @foreach($interviewer->user->experience->sortByDesc('start_date') as $work)
                            <div class="row timeline-details">
                                <div><a class="black-link section-title" href="{{ action('ListingController@getListingPage') }}?companies={{$work->company_name}}">{{$work->company_name}}</a></div>
                                <div class="row interviewer-current-work">
                                    <div>{{$work->title}}</div>
                                </div>
                                <div class="row interviewer-current-work">
                                    <div>{{$work->start_date->formatLocalized('%B %Y')}} - {{$work->end_date->formatLocalized('%B %Y')}}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-xs-offset-1 col-xs-5">
                <div class="row section-panel">
                    <div class="section-title">
                        Education
                    </div>
                    <div class="section-body">
                        @foreach($interviewer->user->education->sortByDesc('start_date') as $education)
                            <div class="row timeline-details">
                                <div class="section-title">{{$education->institution}}</div>
                                <div class="row interviewer-current-work">
                                    <div>{{$education->degree}}</div>
                                </div>
                                <div class="row interviewer-current-work">
                                    <div>{{$education->start_date->formatLocalized('%B %Y')}} - {{$education->end_date->formatLocalized('%B %Y')}}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="row section-panel">
            <div class="section-title">
                User Reviews
            </div>
            <div class="section-body">
                @foreach($interviewer->user->reviewsFor as $review)
                    <div class="row interview-row rating-row">
                        <div class="col-xs-12">
                            <a class="rating-user-name black-link" href="interviewee/profile/{{$review->by_user_id}}">
                                {{$review->byUser->first_name}} {{$review->byUser->last_name}}
                            </a>
                            <div class="star-rating">
                                <?php $r=0 ?>
                                @for($i=1;$i<=intval($review->rating);$i++)
                                    <span class="fa fa-star" data-rating="{{$i}}"></span>
                                @endfor
                                @if(($review->rating - intval($review->rating))>=0.25 && ($review->rating - intval($review->rating))<=0.75)
                                    <span class="fa fa-star-half-o" data-rating="2"></span>
                                    <?php $r=1 ?>
                                @endif
                                @for($i=intval($review->rating)+$r;$i<5;$i++)
                                    <span class="fa fa-star-o" data-rating="{{$i}}"></span>
                                @endfor
                            </div>
                        </div>
                        <div class="col-xs-12">
                            {{$review->text}}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-xs-offset-5">
    <div class="row section-panel">
            <a href="/interviewer/edit/{{$interviewer->id}}" class="btn-default search-btn btn">Edit Profile</a>
        </div>
    </div>
</div>