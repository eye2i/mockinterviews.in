<div class="tab-pane" id="tab_appointments">
    <div class="col-xs-10 col-xs-offset-1">
        <div class="list-group">
            @foreach($interviewer->appointments() as $appointment)
                @if(\Carbon\Carbon::now()->lt(\Carbon\Carbon::parse($appointment->availability->start_timestamp))===true)
                    <a class="list-group-item apptpad">
                        <div class="row interview-row">
                            <div class="interviewer-img col-xs-2">
                                <img src="{{$appointment->interviewee->user->profile_image_url->link()}}">
                            </div>
                            <div class = "col-xs-6">
                                <h4 class="text-left list-group-item-heading">{{$appointment->interviewee->user->first_name}} {{$appointment->interviewee->user->last_name}}</h4>
                                <b><p class="text-left list-group-item-text">Interview for {{$appointment->listing->subcategory->name}}</p></b>
                                <b><p class="text-left list-group-item-text">On {{date('d-m-Y', strtotime($appointment->availability->start_timestamp))}}, From {{date('G:i', strtotime($appointment->availability->start_timestamp))}} to {{date('G:i', strtotime($appointment->availability->end_timestamp))}}</p></b>
                            </div>
                            <div class="pull-right col-xs-4">
                                <div class="btn-danger shortlist-btn btn pull-right">
                                    Cancel
                                </div>
                                <div class="interview-price pull-right">
                                    ₹{{$appointment->listing->price}}
                                </div>
                            </div>
                            {{--<div class = "col-xs-4">--}}
                            {{--<p class="text-left list-group-item-text">Price : <i class="fa fa-inr"></i>{{$appointment->listing->price}}</p>--}}
                            {{--</div>--}}
                        </div>
                    </a>
                @endif
            @endforeach
        </div>
    </div>
</div>
