<div class="tab-pane" id="tab_availability">
    <div class="row">
        <div class="col-xs-12">
            <input type="button" class="pull-right btn yellow-btn" value="Save" id="save-availabilities">
            <div class="pull-left alert alert-info col-xs-11" role="alert">
                Click to toggle or Drag to multi-toggle availabilities.Click Save to update your availability
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="availability-widget">
                <div class="panel panel-default panel-left-absolute">
                    <div class="panel-heading">
                        <div class="legend panel-title">
                            <div class="row"><div class="key bg-green"></div><div  class="key-info">Available</div></div>
                            <div class="row"><div class="key bg-red"></div><div class="key-info">Unavailable</div></div>
                            <div class="row"><div class="key bg-yellow"></div><div class="key-info">Scheduled Appointment</div></div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php
                        $today = Carbon\Carbon::today();
                        $start = $today->copy();
                        $end = $today->copy();
                        $start->addHours(9);
                        $end->addHours(20.5);
                        ?>
                        @while($start<=$end)
                            <div class="left-absolute-column-cell">{{$start->format('h:i A')}}
                                to {{$start->addHour()->format('h:i A')}}</div>
                        @endwhile
                    </div>
                </div>
                <div id="slideshow">
                    <div>
                        <?php $today = Carbon\Carbon::today() ?>
                        @for($i=0;$i<30;$i++)
                            <div class="cal-item panel panel-default @if($i==0) adjust-margin-for-absolute @endif">
                                <div class="panel-heading">
                                    <div class="panel-title rotate">{{ $today->format('jS F')}}</div>
                                </div>
                                <div class="panel-no-padding panel-body">
                                    <?php
                                    $start = $today->copy();
                                    $end = $today->copy();
                                    $start->addHours(9);
                                    $end->addHours(20.5);
                                    ?>
                                    @while($start<=$end)
                                        <div value="{{ $start->timestamp }}-{{$start->copy()->timestamp}}"
                                             class="timeslot @if($start<Carbon\Carbon::now()) disabled
                                                                                          @elseif(isset($appointments_as_unix_timestamp[$start->timestamp])) bg-yellow disabled
                                                                                          @elseif(isset($availabilites_as_unix_timestamp[$start->timestamp])) bg-green
                                                                                          @else bg-red @endif time-padding list-group-item"></div>
                                        <?php $start->addHour() ?>
                                    @endwhile
                                </div>
                            </div>
                            <?php $today->addDay() ?>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>