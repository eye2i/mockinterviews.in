<div class="tab-pane" id="tab_history">
    <div class="col-xs-10 col-xs-offset-1">
        {{--<div class="list-group">--}}
        @foreach($interviewer->appointments() as $appointment)
            @if(\Carbon\Carbon::now()->gt(\Carbon\Carbon::parse($appointment->availability->start_timestamp))===true)
                {{--<a class="list-group-item apptpad">--}}
                <div class="row interview-row">
                    <div class="row">
                        <div class="interviewer-img col-xs-2">
                            <img src="{{$appointment->interviewee->user->profile_image_url->link()}}">
                        </div>
                        <div class = "col-xs-6">
                            <h4 class="text-left list-group-item-heading">{{$appointment->interviewee->user->first_name}} {{$appointment->interviewee->user->last_name}}</h4>
                            <b><p class="text-left list-group-item-text">Interview for {{$appointment->listing->subcategory->name}}</p></b>
                            <b><p class="text-left list-group-item-text">On {{date('d-m-Y', strtotime($appointment->availability->start_timestamp))}}, From {{date('G:i', strtotime($appointment->availability->start_timestamp))}} to {{date('G:i', strtotime($appointment->availability->end_timestamp))}}</p></b>
                        </div>
                        <div class="pull-right col-xs-4">
                            @if($interviewer->user->hasReviewedListing($appointment->listing)===true)
                                <?php $rev = $interviewer->user->getReviewForListing($appointment->listing) ?>
                                <div class="btn-default shortlist-btn btn pull-right upd" id="listing{{$appointment->listing_id}}" rating="{{$rev->rating}}" text="{{$rev->text}}" by="{{$interviewer->user_id}}" for="{{$appointment->interviewee->user_id}}" listing="{{$appointment->listing_id}}">
                                    Update Review
                                </div>
                            @else
                                <div class="btn-default shortlist-btn btn pull-right add" id="listing{{$appointment->listing_id}}" by="{{$interviewer->user_id}}" for="{{$appointment->interviewee->user_id}}" listing="{{$appointment->listing_id}}">
                                    Add Review
                                </div>
                            @endif
                            <div class="interview-price pull-right">
                                ₹{{$appointment->listing->price}}
                            </div>
                        </div>
                    </div>
                </div>
                {{--</a>--}}
            @endif
        @endforeach
        {{--</div>--}}
    </div>

</div>
