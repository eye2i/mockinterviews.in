<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Mock</title>
    @include('head')
</head>

@include('navbar');
<body class="bg">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <center><i class="fa fa-info-circle fa-4x"></i></center>
            <center><h2>About Us</h2></center>
            <h4>We at 'Mock Interviews' help job seekers in finding what is missing in their approach, knowledge or
                skills to ace their interviews.
                We are creating an online platform which connects job seekers with best of their own industry's
                professionals to take their interviews for the profile and role they aim to apply in
                industry. </h4>
            <h3>Mock Interviews</h3>
            <h4>Every professional needs practice, be it sport, studies or an interview. Our goal is to replicate the
                environment and pressure of real job interviews, to help job seekers get the practice
                they need to work on essential aspects which are necessary to stand out in the crowd.
                Job seekers or Interviewees search the 'Mock Interviews' and book an interview with any of available
                industry practitioners and experts. Once booked for an interview, Interview is just a call away. Yes,
                Interview will happen over the phone where Interviewer is an expert who knows the subject and has the
                experience to give feedback and guide one to prepare for next role or job.
                Our interviewers are some of the best in industry. Most of these interviewers have played the role of
                recruitment manager or interviewers in their present or previous companies, They ask
                questions which are typically asked during actual job interviews. They will make special interview
                questions to make you prepared for your aspiration and your next role.</h4>
            <h3>Our Interviewers</h3>

            <h4>We are meeting people from various streams, industries and adding many interviewers to take interviews.
                If you are one of the experts, feel free to write to us.</h4>
        </div>
    </div>
    <div class="row" style="margin-top:100px; margin-bottom: 100px;">
        <div class="col-md-8 col-md-offset-2">
            <center><i class="fa fa-mobile fa-4x"></i></center>
            <center><h2>Contact Us</h2></center>
            <form method="POST" action="{{ URL::action('AuthenticationController@sendContactMail') }}">
                {!! csrf_field() !!}
                <div class="input-group input-group-lg" id="contact_name_ig">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input id="contact_name" class="form-control" type="text" name="name" placeholder="Name">
                </div>
                <div class="input-group input-group-lg" id="contact_email_ig">
                    <span class="input-group-addon"><i class="fa fa-at"></i></span>
                    <input id="contact_email" class="form-control" type="text" name="email" placeholder="Email">
                </div>
                <div class="input-group input-group-lg" id="contact_phone_ig">
                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                    <input id="contact_phone" class="form-control" type="number" name="phone"
                           placeholder="Phone Number (Optional)">
                </div>
                <textarea id="contact_messg" rows="7" class="form-control input-lg" style="margin-top: 10px;"
                          name="message" placeholder="Message"></textarea>
                <input id="contact" type="submit" name="contact" style="margin-top:10px;" value="Submit"
                       class="btn-default search-btn btn">
            </form>
        </div>
    </div>
</div>
@include('footer');
@include('scripts')
<script type="text/javascript" src="{{ URL::asset('js/authentication.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/contact.js') }}"></script>
</body>

</html>