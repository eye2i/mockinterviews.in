<meta name="viewport" content="width=device-width,maximum-scale=1.0">
<link rel="stylesheet" type="text/css" href="{!!URL::asset('css/bootstrap.min.css')!!}">
<link rel="stylesheet" type="text/css" href="{!!URL::asset('css/font-awesome.min.css')!!}">
<link rel="stylesheet" type="text/css" href="{!!URL::asset('css/bootstrap-social.min.css')!!}">
<link rel="stylesheet" type="text/css" href="{!!URL::asset('css/bootstrapdatetimepicker.min.css')!!}">
<link rel="stylesheet" type="text/css" href="{!!URL::asset('css/jquery.timepicker.min.css')!!}">
<link rel="stylesheet" type="text/css" href="{!!URL::asset('css/bootstrap-slider.min.css')!!}">
<link rel="stylesheet" type="text/css" href="{!!URL::asset('css/bootstrap-datepicker.min.css')!!}">
<link rel="stylesheet" type="text/css" href="{!!URL::asset('css/star-rating.min.css')!!}">
<link rel="stylesheet" type="text/css" href="{!!URL::asset('css/fonts.min.css')!!}">
<link rel="stylesheet" type="text/css" href="{!!URL::asset('css/app.css')!!}">
<link rel="stylesheet" type="text/css" href="{!!URL::asset('css/listings.css')!!}">

